#include <arpa/inet.h> // inet_addr()
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h> // bzero()
#include <sys/socket.h>
#include <unistd.h> // read(), write(), close()
#include <pthread.h>
#include <sys/ioctl.h>
#include <net/if.h>

#define MAX 80
#define SERVER_PORT 8080
#define SA struct sockaddr
#define SERVER_IP "172.25.224.1"

int myport;
char ipaddr[20], myusername[20];

void *server_thread(void * sock);
int connect_to_server(int sock);
void talking_to_server(int sock, int servsock);
void request_from_server(int sock, int servsock);
void *listening_thread(void *sock);
void listening(int sockfd);
void hipeer(int sock);
void sending(int peer_port, char *peer_ip);
void s2r(int sockfd);
void login(int servsock);
void log_out(int sock, int servsock);
void setup();

 
 /**
  * int main() calls to setup() and requests user to input port number. Creates
  * and binds socket. Creates 2 threads: listening_thread and server_thread.
 */
int main()
{   
    char buff [MAX];
    int n = 0, choice, peer_port;
    strcpy(myusername, "\0");
    setup();

    printf("Please enter your port number: ");
    while ((buff[n++] = getchar()) != '\n')
        ;
    myport = atoi(buff);


    int sockfd, connfd;
    struct sockaddr_in cliaddr, cli;
 
    // Socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    
    // Assign PORT
    cliaddr.sin_family = AF_INET;
    cliaddr.sin_addr.s_addr = INADDR_ANY;
    cliaddr.sin_port = htons(myport);

    // Binding socket
    if (bind(sockfd, (struct sockaddr *)&cliaddr, sizeof(cliaddr)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    pthread_t thread_a;
    pthread_create(&thread_a, NULL, &listening_thread, &sockfd); // Will continuously listen for incoming msg
    
    pthread_t thread_b;
    pthread_create(&thread_b, NULL, &server_thread, &sockfd); // Will continuously listen for incoming msg
    
    while(1){
        ;
    }
    return 0;
}


void *server_thread(void * sock){
    int sockfd = *((int*)sock);
    for(;;){
        int servsock = connect_to_server(sockfd);
        talking_to_server(sockfd, servsock);
        close(servsock);
        sleep(5);
    }
}


int connect_to_server(int sock){
    char buff[MAX];
    int n = 0, servsock, conn;
    struct sockaddr_in servaddr;
    char cont = 'y';

    // Connecting to server...
    servsock = socket(AF_INET, SOCK_STREAM, 0);
    if (servsock < 0) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else{
        printf("Socket successfully created..\n");
    }

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERVER_IP); 
    servaddr.sin_port = htons(SERVER_PORT);

    while (cont != 'n'){
        if (connect(servsock, (SA*)&servaddr, sizeof(servaddr)) != 0) {
            printf("Connection with the server failed...\n Try again? y/n: ");
            scanf("%c", &cont);
            if (cont == 'n'){
                exit(0);
            }
        }
        else
            break;
    }
    printf("Connected to the server!\n");

    return servsock;
}


void talking_to_server(int sock, int servsock){
    char buff[MAX];
    int n, x;

    printf("Would you like to\t1  login \t2 request data\t3 log out\n Enter choice: ");
    scanf("%d", &x);
    bzero(buff, sizeof(buff));
    sprintf(buff, "%d", x);
    write(servsock, buff, sizeof(buff));

    if (x == 1){
        login(servsock);
    }
    else if(x == 2){
        // requesting peer info
        write(servsock, buff, sizeof(buff));
        printf("\nRequesting peer info from server\n");
        request_from_server(sock, servsock);
    }
    else if (x == 3){
        // logging off
        write(servsock, buff, sizeof(buff));
        write(servsock, myusername, sizeof(myusername));
        close(servsock);
    }
    else{
        // Invalid choice!!
        printf("Invalid input\n");
        talking_to_server(sock, servsock);
    }
}


void request_from_server(int sock, int servsock)
{
    char buff[MAX], cont, peer_ip[MAX];
    int n, k, peer_port;

    bzero(buff, sizeof(buff));
    printf("\nEnter username you wish to contact: ");
    scanf("%s", buff);
    n = write(servsock, buff, sizeof(buff)); // Sending myport number
    
    bzero(buff, sizeof(buff));
    n = read(servsock, buff, sizeof(buff)); 
    
    k = atoi(buff);
    while(k == 0){
        printf("User not found... \nTry other user? (y/n)\n");
        scanf("%c", &cont);
        if (cont == 'n'){
            n = write(servsock, "1", 1);
            
            return;
        }
        if (cont == 'y'){
            request_from_server(sock, servsock);
            return;
        }
    }
    /* Port received from server */
    bzero(buff, sizeof(buff));
    n = read(servsock, buff, sizeof(buff)); 
    peer_port = atoi(buff);

    /* IP address received from server */
    bzero(buff, sizeof(buff));
    n = read(servsock, buff, sizeof(buff)); 
    strcpy(peer_ip, buff);
    sending(peer_port, peer_ip);
}

/**
 * void *listening_thread() calls to function listening() every 5 seconds.
*/
void *listening_thread(void *sock){
    int sockfd = *((int*)sock);
    for(;;){
        sleep(5);
        listening(sockfd);
    }
}

/**
 * void listening() calls function listen(). When a request to connect is 
 * "heard" a new socket is accepted. If connection is accpeted without failure,
 * calls to hipeer(). The connection is then closed.
*/
void listening(int sockfd)
{
    int len, newsockfd, connfd, pid;
    struct sockaddr_in peer;
    // listening for peers
    if ((listen(sockfd, 5)) != 0) {
        printf("Listen failed...\n");
        exit(0);\
    }
    len = sizeof(peer);
    
    while(1){
        newsockfd = accept(sockfd, (struct sockaddr *) &peer, &len);
        if (newsockfd < 0) {
            perror("\nERROR on accept\n");
            exit(1);
        }

        pid = fork();
        if (pid < 0) {
            perror("\nERROR on fork\n");
            exit(1);
        }
        
        if (pid == 0) {
            close(sockfd);
            hipeer(newsockfd);
            exit(0);
        }
        else {
            close(newsockfd);
        }
    }
}

/**
 * void hipeer() sends a hello to connected peer. Then waits to receive a 
 * message.
*/
void hipeer(int sock){
    int n;
    char buff [MAX];
    printf("\npeer connected! \n");
    bzero(buff, sizeof(buff));
    read(sock, buff, sizeof(buff));
    printf("peer: %s\n", buff);
}

/**
 * void sending() requests user to input port to contact. Creates new socket 
 * with the requested port. Calls to connnect(). On success, calls to s2r().
*/
void sending(int peer_port, char *peer_ip){

    char buff[MAX], cont = 'y';
    int n = 0, sock, conn;
    struct sockaddr_in servaddr;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        printf("Socket creation failed...\n");
        exit(0);
    }

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(peer_ip); 
    servaddr.sin_port = htons(peer_port);
    //printf("Peer port given (w/in sending funct): %d\n", peer_port);
    while (cont != 'n'){
        if (connect(sock, (SA*)&servaddr, sizeof(servaddr)) != 0) {
            printf("Connection with peer failed...\n Try again? y/n: ");
            scanf("%c", &cont);
            if (cont == 'n'){
                return;
            }
        }
        else
            break;
    }
    printf("Connected to peer!\n");

    s2r(sock);

}


/**
 * void s2r() waits to receive message from peer. Requests message to send back
 * to peer. Sends the message. Closes socket to peer.
*/
void s2r(int sockfd)
{
    char buff[MAX];
    int n = 0;
    bzero(buff, sizeof(buff));
    printf("Enter your message: ");
    scanf("%s", buff);
    write(sockfd, buff, sizeof(buff));
    printf("\nYou: %s \nCommunication ending\n", buff);
    close(sockfd);
}

void login(int servsock){
    /* Sending server log information... */
    char buff[MAX];
    int y = 0;
    /* Sending port number */
    bzero(buff, sizeof(buff));
    sprintf(buff, "%d", myport);
    write(servsock, buff, sizeof(buff)); 
    /* Sending IP address */
    bzero(buff, sizeof(buff));
    strcpy(buff, ipaddr);
    write(servsock, buff, sizeof(buff));
    /* Selecting username */
    while(y == 0){
        bzero(buff, sizeof(buff));
        read(servsock, buff, sizeof(buff)); 
        printf("%s\t", buff); // "Enter username: "

        bzero(buff, sizeof(buff));
        scanf("%s", buff);
        write(servsock, buff, sizeof(buff)); // Sending username
        // reading to see if input was valid
        read(servsock, buff, sizeof(buff)); 
        y = atoi(buff);
        if (y == 0){ printf("Username taken... \n");}
    }
    printf("Login successful!\n");
    strcpy(myusername, buff);
    close(servsock);
}

void log_out(int sock, int servsock){
    //CONTACT SERVER AND REQ TO LOG OUT
    char buff[MAX];
    write(servsock, myusername, sizeof(myusername));
    bzero(buff, sizeof(buff));
    read(servsock, buff, sizeof(buff));
    if (atoi(buff) == 0){
        printf("Logged off\n");
        close(servsock);
        close(sock);
        exit(0);
    }
    else{
        printf("Error logging off.\n");
        close(servsock);
        close(sock);
        exit(-1);
    }
}

void setup(){
    int fd;
    struct ifreq ifr;
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);
    printf("Host Device's IP Addr: %s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    strcpy(ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}