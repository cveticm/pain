#include <arpa/inet.h> // inet_addr()
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h> //bzero(), strncpy(), strcpy()
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h> // read(), write(), close()
#include <pthread.h>
#include <sys/ioctl.h>
#include <net/if.h>
#define MAX 80
#define PORT 8080
#define SA struct sockaddr
#define ARRAY_SIZE 100

typedef struct user{
    int port;
    char name[20], ip[20];
    struct user *next;
}User;

User *peer[ARRAY_SIZE];
pthread_mutex_t mut;
char ipaddr[20];

User *search(char *name);
User **search_availability(char *name);
void new_user(int newsock);
void receive_request(int peersock);
void *listening_thread(void *sock);
void listening(int sockfd);
void talking_to_user(int usersock);
void logout(int usersock);
int hash(char *name);
void setup();

int main()
{
    int sockfd, connfd, len, newsockfd, pid;
    struct sockaddr_in servaddr, cli;
    memset(peer, 0, sizeof(peer)); // Setting all elements in hash table *Peer to NULL
    setup(); // Getting and storing IP addr of server
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);// First call to socket() funct
    // Checking for socket creation failure
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else{
        printf("Socket successfully created..\n Sockfd = %d\n", sockfd);
    }
    // Initializing socket structure
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);
   
    // Binding newly created socket to given IP and verification 
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        printf("Failed to bind socket...\n");
        exit(0);
    }
    else{
        printf("Successfully binded socket!\n");
    }
    
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, &listening_thread, &sockfd); // Will continuously listen for incoming msg
    while(1){
        ;
    }
}


User *search(char *name)
{
    /* Searches through array hashTable for given name. Calculates key and
     * jumps to that point in the array and counts number of occurances of the
     * name, updating key by + (number of not NULL terms encounter) ^ 2 , until
     * a NULL is encountered */
    printf("Searching for %s\n", name);
    int key = hash(name);
    User *check = peer[key];
    while (check != NULL) // if string is empty/ only contains 0, then length of string (strlen) = 0
    {
        if (strcmp(check->name, name) == 0)
        {
            printf("%s found.\n", name);
            printf("%s port is %d\n", name, check->port);
            return check;
        }
        else{
            check = check->next;
        }
    }
    printf("%s not found.\n", name);
    return NULL;
}
/** Searches in key if username is already recorded in array. Returns -1 if 
 * username is taken and address of list head if availible.
 */
User **search_availability(char *name)
{
    
    int key = hash(name);
    for (;;)
    {
        User *check = peer[key];
        if (check == NULL){ // if string is empty, then length of string (strlen) = 0
            return &peer[key];
        }
        else if (strcmp(check->name, name) == 0){
            check = check->next;
        }
        else{
            return NULL;
        }
    }
}

User * new_item(char *name, int port, char *ip){
    int key = hash(name);
    User *item = (User *)malloc(sizeof(User));
    strcpy(item->name, name);
    printf("new_item port: %d\n", port);
    item->port = port;
    strcpy(item->ip, ip);
    item->next = NULL;
    return item;
}

/**
 * 
*/
void new_user(int newsock)
{
    char buff[MAX], new_name[MAX], new_ip[MAX];
    int n, new_port;

    bzero(buff, sizeof(buff));
    n = read(newsock, buff, sizeof(buff));
    new_port = atoi(buff);

    bzero(buff, sizeof(buff));
    n = read(newsock, buff, sizeof(buff));
    strcpy (new_ip, buff);
    printf("ipaddr received: %s\n", new_ip);

    while(1){
        printf("Requesting username...\n");
        n = write(newsock, "Enter username: ", 16);

        bzero(new_name, sizeof(new_name));
        n = read(newsock, new_name, sizeof(buff));

        User **avb = search_availability(new_name);
        if (avb == NULL){
            printf("Username, taken.\n");
            n = write(newsock, "0", 1);
        }
        else 
        {
            printf("new_user port: %d\n", new_port);
            User *pStruct = new_item(new_name, new_port, new_ip);
            pthread_mutex_lock(&mut);
            pStruct->next = *avb;
            *avb = pStruct;
            pthread_mutex_unlock(&mut);
            printf("Username, logged.\n");
            n = write(newsock, "1", 1);
            break;
        }
    }
}

/**
 * 
*/
void receive_request(int peersock)
{
    User *request;
    char buff[MAX];
    int n, k = 0, key;

    //searching for user
    while(k == 0){
        bzero(buff, sizeof(buff));
        n = read(peersock, buff, sizeof(buff)); // getting requested username
        request = search(buff);
        
        if (request == NULL){ // if string is empty, then length of string (strlen) = 0
            n = write(peersock,"0", 1);
            
            bzero(buff, sizeof(buff));
            n = read(peersock, buff, sizeof(buff)); // get if user wishes to search another username
            k = atoi(buff);
        }
        else{
            n = write(peersock, "1", 1); // Tells client user was found.
            printf("requested user port: %d\n", request->port);
            bzero(buff, sizeof(buff));
            sprintf(buff, "%d", request->port);
            n = write(peersock, buff, sizeof(buff));
            
            bzero(buff, sizeof(buff));
            //n = write(peersock, buff, sizeof(buff));
            
            //printf("Sending ip addr\n");
            n = write(peersock, request->ip, sizeof(request->ip));
            break;
        } 
    }
}

/**
 * void *listening_thread() calls to function listening() every 5 seconds.
*/
void *listening_thread(void *sock){
    int sockfd = *((int*)sock);
    for(;;){
        sleep(5);
        listening(sockfd);
    }
}

void *talking_thread(void *newsock){
    int newsockfd = *((int*)newsock);
    talking_to_user(newsockfd);
}

/**
 * void listening() calls function listen(). When a request to connect is 
 * "heard" a new socket is accepted. If connection is accpeted without failure,
 * calls to new_user(). The connection is then closed.
 * ***PID MAY BE REDUNDANT CURRENTLY***
*/
void listening(int sockfd)
{
    int len, newsock, connfd, pid;
    struct sockaddr_in peersock;
    // listening for clients
    if ((listen(sockfd, 5)) != 0) {
        printf("Listen failed...\n");
        exit(0);
    }
    // else
    //     printf("Server listening..\n");
    len = sizeof(peer);
    
    while(1){
        newsock = accept(sockfd, (struct sockaddr *) &peersock, &len);
        if (newsock < 0) {
            perror("\nERROR on accept\n");
            exit(1);
        }

        pthread_t thread_b;
        pthread_create(&thread_b, NULL, &talking_thread, &newsock);
        // pid = fork();  
        // if (pid < 0) {
        //     perror("\nERROR on fork\n");
        //     exit(1);
        // }
        // if (pid == 0) { 
        //     close(sockfd);
        //     talking_to_user(newsock, peersock);
        //     exit(0);
        // }
        // else {
        //     close(newsock);
        // }
    }
}

/**
 * 
*/
void talking_to_user(int usersock){
    char buff[MAX];
    int n, x;

    // User requests for peer info or to logout.
    bzero(buff, sizeof(buff));
    n = read(usersock, buff, sizeof(buff));
    
    x = atoi(buff);
    
    // Calling to appropriate function.
    if (x == 1){
        // Log in client
        printf("Logging in client\n");
        new_user(usersock);        
    }
    else if (x == 2){
        // Peer info
        printf("\nUser requesting data of other user.\n");
        receive_request(usersock);
    }
    else if(x == 3){
        // Logout
        printf("User requesting to log off.\n");
        logout(usersock);
    }
    else{
        // Invalid option.
        // Client will be requested to input a valid option.
        talking_to_user(usersock);
    }
}

int delete(char *name){
    int key = hash(name);
    User *check = peer[key];
    User *prev = peer[key];
    while (check != NULL) // if string is empty/ only contains 0, then length of string (strlen) = 0
    {
        if (strcmp(check->name, name) == 0)
        {
            pthread_mutex_lock(&mut);
            prev->next = check->next;
            free(check);
            pthread_mutex_unlock(&mut);
            return 0;
        }
        else{
            prev = check;
            check = check->next;
        }
    }
    return 1;
}
/**
 * 
*/
void logout(int usersock){
    User *client;
    char buff[MAX];
    bzero(buff, sizeof(buff));
    read(usersock, buff, sizeof(buff));

    if (delete(buff) == 0){

        printf("User %s successfully logged off.\n", buff);
        write(usersock, "0", 1);
        close (usersock);
    }
    else{
        printf("Error logging off user %s\n", buff);
        write(usersock, "-1", sizeof("-1"));
        exit(-1);
    }

}

int hash(char *name){
    /* Calculates a key to decide position in array */
    int hash = 0;
    while (*name)
    {
        hash = hash + *name;
        name++;
    }
    return (hash % ARRAY_SIZE); //"key" - also, % ARRAY_SIZE just ensures that sum of ascii values bigger than array_size, dont escape the array
}


void setup(){
    int fd;
    struct ifreq ifr;
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);
    printf("Host Device's IP Addr: %s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    strcpy(ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    return;
}
